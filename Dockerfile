FROM debian:buster-20190326-slim AS builder


MAINTAINER Per Weijnitz


ARG leptonica_version=v1.74.3
ARG openalpr_version=v2.3.0

ARG gstreamer_version=1.4
#ARG gstreamer_version=1.14.4

ARG opencv_version=3.4.4
#ARG opencv_version=3.4.5

ARG tesseract_version=3.05.02
#ARG tesseract_version=4.0.0



RUN apt-get update &&\
    apt-get install -y \
    autoconf   \
    automake   \
    beanstalkd   \
    bison \
    build-essential   \
    cmake   \
    cmake-curses-gui   \
    cppcheck   \
    flex \
    git   \
    git-core   \
    gtk-doc-tools \
    libatlas-base-dev    \
    libatlas3-base   \
    libavcodec-dev \
    libavcodec58 \
    libavformat-dev   \
    libboost-python-dev    \
    libcairo2-dev   \
    libcurl4-gnutls-dev \
    libdb5.3-dev   \
    libdc1394-22-dev   \
    libflann-dev   \
    libgflags-dev   \
    libgoogle-glog-dev   \
    libgphoto2-6 \
    libgtk-3-0 \
    libgtk2.0-dev   \
    libhdf5-dev   \
    libjpeg-dev   \
    libjpeg62-turbo-dev   \
    libjson-pp-perl   \
    libleveldb-dev    \
    liblmdb-dev   \
    liblog4cplus-dev   \
    liborc-0.4-dev \
    libpango1.0-dev   \
    libpangocairo-1.0-0   \
    libpng-dev   \
    libprotobuf-dev   \
    libswscale-dev   \
    libtbb-dev   \
    libtbb2   \
    libtiff-dev   \
    libtiff5-dev   \
    libtool   \
    libx264-dev   \
    parallel   \
    pkg-config   \
    pngcrush   \
    protobuf-c-compiler   \
    protobuf-compiler   \
    pybind11-dev \
    pylint   \
    python-dev   \
    python-gflags   \
    python-gobject-2-dev \
    python-numpy   \
    python-pandas   \
    python-protobuf   \
    python-tables   \
    python-tables-lib    \
    stow \
    sudo \
    x11-xserver-utils \
    yasm \
    zlib1g-dev



## Build GStreamer
RUN for P in gstreamer gst-plugins-base gst-plugins-good gst-libav gst-plugins-bad gst-plugins-ugly gst-rtsp-server  ; do \
    cd /tmp && \
    if [ ! -d $P ]; then \
        git clone https://github.com/GStreamer/"$P".git ; \
    fi ; \
    export EXTRAFLAGS= ; \
    cd $P && \
        git pull && git checkout $gstreamer_version && \
        bash autogen.sh --prefix=/usr/stow/gstreamer --with-pkg-config-path=/usr/stow/gstreamer/lib/pkgconfig $EXTRAFLAGS && \
        ./configure --prefix=/usr/stow/gstreamer --with-pkg-config-path=/usr/stow/gstreamer/lib/pkgconfig $EXTRAFLAGS && \
        make -j4 && make install && touch /usr/stow/"$P".done && cd /tmp && rm -rf "$P" ; \
        done ;\
  stow --dir=/usr/stow gstreamer &&\
  rm -rf /tmp/gst-plugins-ugly



## Build OpenCV
RUN cd /tmp && \
    curl -L -o $opencv_version.tar.gz https://github.com/opencv/opencv/archive/$opencv_version.tar.gz &&\
    tar -zxvf $opencv_version.tar.gz &&\
    mv opencv-$opencv_version opencv &&\
    mkdir /tmp/build_opencv && \
    cd /tmp/build_opencv && \
    cmake -D CMAKE_INSTALL_PREFIX=/usr/stow/opencv -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D OPENCV_BUILD_3RDPARTY_LIBS=ON -D WITH_FFMPEG=ON -D WITH_GTK=ON -D WITH_OPENEXR=ON -D WITH_OPENNI=ON -D WITH_PNG=ON -D WITH_TBB=ON -D WITH_XINE=ON -D WITH_GSTREAMER=ON -D BUILD_SAMPLES=ON /tmp/opencv && \
    export NUMPROC=$(nproc --all) &&\
    make -j$NUMPROC &&\
    make -j$NUMPROC install &&\
    stow --dir=/usr/stow opencv &&\
    cd /tmp && rm -rf /tmp/opencv /tmp/$opencv_version.tar.gz  /tmp/build_opencv



## Build Leptonica
RUN cd /tmp && \
    git clone https://github.com/DanBloomberg/leptonica.git && \
    cd leptonica && \
    git checkout $leptonica_version &&\
    automake --add-missing ; libtoolize ; autoreconf ; make clean ; ./configure --prefix=/usr/stow/liblept ; \
    automake --add-missing ; libtoolize ; autoreconf ; make clean ; ./configure --prefix=/usr/stow/liblept ; \
    make -j4 && \
    make install && \
    stow --dir=/usr/stow liblept &&\
    cd /tmp && rm -rf leptonica



## Build Tesseract
RUN cd /tmp && \
    curl -o $tesseract_version.tar.gz https://codeload.github.com/tesseract-ocr/tesseract/tar.gz/$tesseract_version && \
    tar -zxf $tesseract_version.tar.gz && \
    cd tesseract-$tesseract_version && \
    ./autogen.sh && \
    ./configure --prefix=/usr/stow/tesseract --disable-graphics && \
    make -j4 && \
    make install && \
    curl -o /usr/stow/tesseract/share/tessdata/osd.traineddata https://raw.githubusercontent.com/tesseract-ocr/tessdata/master/osd.traineddata && \
        curl -o /usr/stow/tesseract/share/tessdata/swe.traineddata https://raw.githubusercontent.com/tesseract-ocr/tessdata/master/swe.traineddata && \
    stow --dir=/usr/stow tesseract &&\
    cd /tmp && rm -rf $tesseract_version.tar.gz tesseract-$tesseract_version



## Build OpenALPR
RUN cd /tmp && \
    git clone https://github.com/openalpr/openalpr.git && \
    cd openalpr && \
    git checkout $openalpr_version &&\
    cmake -D CMAKE_INSTALL_PREFIX=/usr/stow/openalpr src && \
    make -j4 && \
    make install &&\
    stow --dir=/usr/stow openalpr &&\
    cd /tmp && rm -rf openalpr



## Create artefact and cleanup
RUN cd /usr && \
    tar -zcf stow.tgz stow &&\
    apt-get -y autoremove &&\
    apt-get -y clean &&\
    rm -rf /usr/src /var/lib/apt/lists/*




FROM debian:buster-20190326-slim AS artefact

MAINTAINER Per Weijnitz

COPY --from=builder /usr/stow.tgz /usr/stow.tgz

ADD src /tmp/src

RUN apt-get update &&\
    apt-get install -y --no-install-recommends \
    beanstalkd   \
    build-essential\
    libatlas3-base \
    libavcodec-extra \
    libavcodec-extra58 \
    libcurl4-gnutls-dev \
    libdc1394-22 \
    libglib2.0-dev \
    libgphoto2-6 \
    libgtk-3-0 \
    libgtk2.0-0 \
    libjson-pp-perl   \
    liborc-0.4-dev  \
    libpangocairo-1.0-0   \
    libswscale-dev \
    libtbb2   \
    libtool \
    libx11-dev \
    libxine2 \
    libxine2-all-plugins \
    libxine2-x \
    make \
    parallel \
    pkg-config \
    pngcrush \
    pylint \
    python-gflags   \
    python-numpy   \
    python-pandas   \
    python-protobuf   \
    python-tables   \
    python-tables-lib    \
    stow \
    sudo \
    time \
    x11-xserver-utils && \
    tar -C /usr -zxf /usr/stow.tgz && \
    cd /usr/stow && find . -maxdepth 1 -type d  -not -path '\.' | tr -d '[/.]' | xargs -L 1 stow --dir=/usr/stow && \
    cd /tmp/src && \
    make &&\
    apt-get -y autoremove &&\
    apt-get -y clean &&\
    rm -f /usr/stow.tgz && \
    rm -rf /var/lib/apt/lists/*
