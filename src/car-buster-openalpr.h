#ifndef CAR_BUSTER_OPENALPR_H
#define CAR_BUSTER_OPENALPR_H


void init(char *_country, char *_config, char *_runtime_data);
std::string analyse(unsigned char* pixelData, int bytesPerPixel, int imgWidth, int imgHeight);


#endif // CAR_BUSTER_OPENALPR_H
