#include <alpr.h>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <unistd.h>


#define THRES 84.0


char* country      = NULL;
char* config       = NULL;
char* runtime_data = NULL;
alpr::Alpr* openalpr = NULL;


void init(char *_country, char *_config, char *_runtime_data) {
  country      = _country;
  config       = _config;
  runtime_data = _runtime_data;
  openalpr = new alpr::Alpr(country, config);
  openalpr->setTopN(1);

  if (openalpr->isLoaded() == false)
    {
      std::cerr << "Error loading OpenALPR" << std::endl;
      return;
    }

  openalpr->setTopN(1);
}



#include <opencv2/core/core.hpp>

std::string analyse(unsigned char* pixelData, int bytesPerPixel, int imgWidth, int imgHeight) {

  alpr::AlprResults results = openalpr->recognize(pixelData, bytesPerPixel, imgWidth, imgHeight, std::vector<alpr::AlprRegionOfInterest>());


  // Iterate through the results.  There may be multiple plates in an image,
  // and each plate return sthe top N candidates.

  std::string retval = "";
  if(results.plates.size() > 0) {
    for (int i = 0; i < results.plates.size(); i++)
      {
        alpr::AlprPlateResult result = results.plates[i];
        alpr::AlprPlate plate = result.bestPlate;
        //std::cerr << "PLATE: " << plate.characters << "\n";
        retval += "{ \"candidates\":[";
        retval +=     "{ \"plate\":\"" + plate.characters + "\", "
          +      " \"confidence\": \"" + std::to_string(plate.overall_confidence) + "\", "
                  +      " \"passed\": \"" + ( plate.overall_confidence >= THRES ? "1" : "0" ) + "\" ";
        retval +=     "} ], \"coordinates\":[";
        for(int y = 0; y < 4; y++) {
          retval +=   "{ \"x\":\"" + std::to_string(result.plate_points[y].x) + "\","
            +     "\"y\":\""  + std::to_string(result.plate_points[y].y) + "\""
                    +   "}";
          if(y < 3) {
            retval += ",";
          }
        }
        retval +=                 "] ";
        //retval += " , \"image\":\"" + file + "\"";
        retval += " }\n";
      }
  }

  return retval;
}
