// http://stackoverflow.com/questions/37339184/how-to-write-opencv-mat-to-gstreamer-pipeline
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <stdlib.h>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <gstreamer-1.0/gst/video/video.h>
#include <gstreamer-1.0/gst/gst.h>
#include <gstreamer-1.0/gst/app/gstappsink.h>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <X11/Xlib.h>

#include "car-buster-openalpr.h"


#define MSG(...)        do { fprintf (stderr, __VA_ARGS__); } while(0)
#define ERR(...)        do { MSG (__VA_ARGS__); } while(0)
#define DIE(...)        do { ERR(__VA_ARGS__); exit(EXIT_FAILURE); } while(0)
#define DIE_PERROR(...) do { perror(0); DIE(__VA_ARGS__); } while(0)



using namespace cv;


bool time_to_exit = false;

std::deque<Mat> frameQueue;

std::mutex m;
#define LOCK   std::unique_lock<std::mutex> lock(m)
#define UNLOCK lock.unlock()


char * alprgeo  = "eu";
char * alprconf = "/tmp/proj/container/alprd.conf";
char * alprrt   = "/usr/local/share/openalpr/runtime_data";


void frame_eater()
{
  init(alprgeo, alprconf, alprrt);

  namedWindow("edges",1);

  while(! time_to_exit) {

    LOCK;
    if (frameQueue.size() > 0) {
      Mat frame = frameQueue.front();

      if(frame.rows > 0) {
        std::string result = analyse(frame.data, frame.elemSize(), frame.size().width, frame.size().height);
        if(result.length() > 0) {
          std::cout << "RESULT: " << result << "\n";
          imshow("edges", frame);
          cv::waitKey(30);
        }
      }

      //std::cerr << "framesize: " << (( frame.total() * frame.elemSize() ) / 1024 ) << "kB\tskipping frames: " << frameQueue.size() - 1 << "\n";

      // On slow machines, we can stay in phase with the latest incoming frames by
      // discarding frames that have built up during processing the current frames:
      frameQueue.clear();

      // TODO: free all allocated frame objects, including dropped ones.

      UNLOCK;
    } else {
      UNLOCK;
      usleep(100);
    }
  }
}




int main(int argc, char** argv) {
  cv::Mat frame;
  int key;
  std::string gstline = "filesrc location=../data/IMG_0494.MOV ! qtdemux ! decodebin ! videorate ! videoscale ! video/x-raw, width=960, height=600, framerate=25/1 ! autovideoconvert ! appsink";

  // Other interesting options include static images and web cameras:
  //cv::VideoCapture cap("filesrc location=/home/perweij/jobb/ocr/bin/bgr_sgv_big.png ! pngdec ! appsink");
  //cv::VideoCapture cap("v4l2src device=/dev/video0 ! videoconvert ! appsink");
  //cv::VideoCapture cap("filesrc location=/home/perweij/jobb/ocr/bin/bgr_sgv_big.png ! pngdec ! videoconvert ! imagefreeze ! appsink");

  int c;
  while ((c = getopt (argc, argv, "g:l:c:r:")) != -1)
    switch (c)
      {
      case 'g':
        gstline = optarg;
        break;
      case 'l':
        alprgeo = optarg;
        break;
      case 'c':
        alprconf = optarg;
        break;
      case 'r':
        alprrt = optarg;
        break;
      default:
        abort ();
      }

  XInitThreads();

  cv::VideoCapture cap(gstline);

  if (!cap.isOpened()) {
    DIE("=ERR= can't create video capture\n");
  }

  std::thread t1(frame_eater);
  std::thread t2(frame_eater);
  std::thread t3(frame_eater);

  while (true) {
    cap >> frame;
    if (frame.empty()) {
      break;
    }
    LOCK;
    frameQueue.push_front(frame);
    UNLOCK;
  }
  time_to_exit = true;
  t1.join();
  t2.join();
  t3.join();
}
