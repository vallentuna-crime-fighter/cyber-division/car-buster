## Name of the generated Docker image
DOCKERIMG := car-buster-v0



## Path to the Dockerfile
DOCKERFILE := Dockerfile



## Path to this project folder in a running container
PROJMOUNT = /proj



## For settings shared between this Makefile and src/Makefile
include src/Makefile.settings



## Command to launch a container
## You may want these when experimenting with hardware cameras
# --device=/dev/snd
# --device=/dev/video0
RUNCMD = docker run -v $(PWD)\:$(PROJMOUNT) --rm --cap-add SYS_PTRACE \
         -v /tmp/.X11-unix\:/tmp/.X11-unix -e DISPLAY \
         --net=host \
	 --ipc=host \
         -i -t $(DOCKERIMG)



## Command to run the test program
TSTCMD = TESSDATA_PREFIX=/usr/stow/openalpr/share/openalpr/runtime_data/ocr/tessdata LD_LIBRARY_PATH=/usr/stow/gstreamer/lib /usr/bin/$(BIN)



.PHONY: builddocker
builddocker:
	docker build --no-cache -f $(DOCKERFILE) -t $(DOCKERIMG) .



.PHONY: test
test:
	xhost +
	time $(RUNCMD) bash -c "cd $(PROJMOUNT) && make test_in_container"



.PHONY: rebuild_then_test
rebuild_then_test:
	xhost +
	time $(RUNCMD) bash -c "cd $(PROJMOUNT) && make -C src all && make test_in_container"



## Target to run within a container to start the test
.PHONY: test_in_container
test_in_container:
	cp /proj/conf/alprd.conf /usr/stow/openalpr/share/openalpr/config/openalpr.defaults.conf
	$(TSTCMD) -g 'filesrc location=testdata/test0.mov ! qtdemux ! decodebin ! videorate ! videoscale ! video/x-raw, width=960, height=600, framerate=25/1 ! autovideoconvert ! appsink'




## Start an X enabled shell
xbash:
	xhost +
	xterm -e $(RUNCMD) /bin/bash -i



## Quick way to jump into a container and investigate
bash:
	$(RUNCMD) /bin/bash -i



## Cleanup old junk from the Docker store
clean:
	docker container prune -f
	docker images -q --filter "dangling=true" | xargs -r docker rmi
